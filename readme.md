# BC Legal PHP Test

You're client is a Pizza shop. They have a large menu of pizza's that each require some slightly different method to make. 
Each pizza is then baked in an oven for a different number of minutes depending on pizza and the type of oven.

Your task is to create a series of classes that can represent the following pizza's and ovens.

A run time you should be able to easily choose your pizza and oven type and bake you're pizza.

## Pizza
Pizza              | Topping        | Bake for (in Gas Oven)
------------------ | -------------- | ------------------------
Cheese and Tomato  | Tomatoes       | 20 Mins
Chicken Supreme    | Chicken        | 22 Mins
Meat Feast         | Chicken & Beef | 25 Mins

## Ovens
Oven               | Adjust Bake Time
------------------ | ------------------------
Gas Oven           | n/a
Stone Oven         | Subtract 10 minutes
Hair Dryer         | Add 1 hour

## Deliverable's

When I run `php make-pizza.php` I should see all combination of pizza and ovens displayed in a human readable way in the console.

e.g.
```
*************************
--- Cheese And Tomato ---
*************************

Dough rolled out.
Tomato sauce spread.
Tomatoes added.
Cheese sprinkled on.
Baked in gas oven for for 20 minutes
```

You should create a series of classes to help solve the problem above. You should use the best design principles you can to allow for future changes.
You should adjust `make-pizza.php` as your client and it should be updated to demonstrate your solution.