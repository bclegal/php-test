<?php

require 'vendor/autoload.php';

//Choose type of pizza at run time.
$pizza = new \PizzaShop\Pizza\CheeseAndTomato;

//Make the pizza.
$pizza->make();

//Choose type of oven at run time.
$oven = new \PizzaShop\Oven\GasOven;

//Bake the pizza.
$oven->bake($pizza);