<?php namespace PizzaShop\Pizza;

/**
 * A cheese and tomato pizza.
 */
class CheeseAndTomato
{

    /**
     * Make pizza.
     *
     * @return $this
     */
    public function make()
    {
        return $this
            ->rollOutDough()
            ->spreadTomatoSauce()
            ->addTomatoes()//<--- This will change
            ->sprinkleOnCheese();
    }


    public function rollOutDough()
    {
        print 'Dough rolled out.'.PHP_EOL;

        return $this;
    }

    public function spreadTomatoSauce()
    {
        print 'Tomato sauce spread.'.PHP_EOL;

        return $this;
    }

    public function addTomatoes()
    {
        print 'Tomatoes added.'.PHP_EOL;

        return $this;
    }

    public function sprinkleOnCheese()
    {
        print 'Cheese sprinkled on.'.PHP_EOL;

        return $this;
    }
}